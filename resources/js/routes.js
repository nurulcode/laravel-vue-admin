const login = require('./components/auth/Login.vue').default;
const register = require('./components/auth/Register.vue').default;


export const routes = [
    { path: '/', component: login, name: '/' },
    { path: '/register', component: register, name: 'register' }
  ]
